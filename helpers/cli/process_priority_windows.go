package cli_helpers

import (
	"syscall"

	"github.com/pkg/errors"
)

const (
	// https://msdn.microsoft.com/en-us/library/windows/desktop/ms686219(v=vs.85).aspx
	aboveNormalPriorityClass   = 0x00008000
	belowNormalPriorityClass   = 0x00004000
	highPriorityClass          = 0x00000080
	idlePriorityClass          = 0x00000040
	normalPriorityClass        = 0x00000020
	processModeBackgroundBegin = 0x00100000
	processModeBackgroundEnd   = 0x00200000
	realtimePriorityClass      = 0x00000100
)

// SetIdlePriority lowers the process CPU scheduling priority, and possibly
// I/O priority depending on the platform and OS.
func SetIdlePriority() error {
	return setPriority(idlePriorityClass)
}

// SetLowPriority lowers the process CPU scheduling priority, and possibly
// I/O priority depending on the platform and OS.
func SetLowPriority() error {
	return setPriority(belowNormalPriorityClass)
}

// SetNormalPriority resumes normal process CPU scheduling priority, and possibly
// I/O priority depending on the platform and OS.
func SetNormalPriority() error {
	return setPriority(normalPriorityClass)
}

// SetHighPriority raises the process CPU scheduling priority, and possibly
// I/O priority depending on the platform and OS.
func SetHighPriority() error {
	return setPriority(highPriorityClass)
}

// SetLowPriority lowers the process CPU scheduling priority, and possibly
// I/O priority depending on the platform and OS.
func setPriority(prio uintptr) error {
	modkernel32 := syscall.NewLazyDLL("kernel32.dll")
	setPriorityClass := modkernel32.NewProc("SetPriorityClass")

	if err := setPriorityClass.Find(); err != nil {
		return errors.Wrap(err, "find proc")
	}

	handle, err := syscall.GetCurrentProcess()
	if err != nil {
		return errors.Wrap(err, "get process handler")
	}
	defer syscall.CloseHandle(handle)

	res, _, err := setPriorityClass.Call(uintptr(handle), prio)
	if res != 0 {
		// "If the function succeeds, the return value is nonzero."
		return nil
	}
	return errors.Wrap(err, "set priority class") // wraps nil as nil
}
