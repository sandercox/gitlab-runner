package cli_helpers

import (
	"github.com/urfave/cli"
)

func SetupProcessPriority(app *cli.App) {
	app.Flags = append(app.Flags, cli.StringFlag{
		Name:   "priority",
		Usage:  "process priority (idle, low, normal, high)",
		EnvVar: "PROC_PRIORITY",
	})

	appBefore := app.Before

	app.Before = func(c *cli.Context) error {
		// Fix home
		switch c.String("priority") {
		case "idle":
			SetIdlePriority()
		case "low":
			SetLowPriority()
		default:
			panic("Unrecognized process priority")
		}

		if appBefore != nil {
			return appBefore(c)
		}
		return nil
	}
}
